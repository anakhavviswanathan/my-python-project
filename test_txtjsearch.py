# tests for txtjsearch
import pytest
import os
import tempfile
#from jsearch import jsearch
from txtjsecrch import search,jsearch,preaparecontent,getfiles
import json


def test_rejectinvalidjson_withValueError():
    with pytest.raises(ValueError):
        jsearch('{invalid json}').searchanykey(searchTerm="blah")


def test_searchWithExistingKey_returnTrue():
    content = '{"blah":1, "foo":2, "bar":3}'
    result = jsearch(content).searchanykey(searchTerm="blah")
    assert (result == True)
def test_searchWithNonExistingkey_returnFalse():
    content = '{"blah":1, "foo":2, "bar":3}'
    result = jsearch(content).searchanykey(searchTerm="thiskeydosentexist")
    assert (result == False)


def test_searchKeyinsidekey_WithExistingkey_returnTrue():
    content = '{"foo":{"blah":1}, "bar":3}'
    result = jsearch(content).searchanykey("blah")
    assert (result == True)
def test_searchKeyinsidekey_WithNonExistingkey_returnFalse():
    content = '{"foo":{"blah":1}, "bar":3}'
    result = jsearch(content).searchanykey("egg")
    assert (result == False)

def test_searchKeyinsidekey_level5_WithExistingkey_returnTrue():
    content = '{"foo":{"blah":{"bar":{"egg":{"level5":1}}}}, "jag":2}'
    result = jsearch(content).searchanykey("level5")
    assert (result == True)

def test_searchKeyinsidekey_level5_WithNonExistingkey_returnFalse():
    content = '{"foo":{"blah":{"bar":{"egg":{"level5":1}}}}, "jag":2}'
    result = jsearch(content).searchanykey("level6_Doesnotexist")
    assert (result == False)


def test_searchKeyinsidekey_Actualexample_returnTrue():
    content = json.JSONEncoder().encode({
        "name": "TempSensor",
        "fwver": "1.2.0",
        "updates": ["24 June 2022", "20 July 2022", "25 Aug 2022"],
        "services": {
            "gettemp": 1,
            "getunits": 2,
            "switchon": 3,
            "switchoff": 4,
            "language": ["English", "German"]
        }
    })
    result = jsearch(content).searchanykey("getunits")
    assert (result == True)

def test_generateListofKeysWithexistingValue_retunTrue():
    content = '{"foo":{"blah":1}, "bar":3}'
    allkeysdict = jsearch(content).flattenkeys()
    print(allkeysdict)
    assert (allkeysdict != None)
    assert ("blah" in allkeysdict)
    assert ("bar" in allkeysdict)
    assert ("foo" in allkeysdict)
    assert (not "invalidkey" in allkeysdict)

searchTerm=[("blah"),("foo"),("jag"),("egg"),("bar"),("level5")]
@pytest.mark.parametrize('searchTerm',searchTerm)
def test_generateListofKey_OfLevel5json_returnTrue(searchTerm):
    content = '{"foo":{"blah":{"bar":{"egg":{"level5":1}}}}, "jag":2}'
    result = jsearch(content).searchanykey(searchTerm)
    print(result)
    assert result==True


def test_gererateListofkeyInvalidPattern_rejectWithValueError():
    with pytest.raises(ValueError):
        content = '{"foo":{"blah":1}{"bey":3} "bar":3}'
        allkeysdict = jsearch(content).flattenkeys()
        print(allkeysdict)
#json value search reject with TypeError
@pytest.mark.skip(reason="this feature will be implement on the next version")
def test_jsonsearchWith_Jsonvalue_rejectedWithTypeError():
    with pytest.raises(TypeError):

       content = {"room-number": 101,
         "use": "waiting",
         "sq-ft": 250,
         "price": 75
       }
       allkeysdict = jsearch(content).flattenkeys()
       assert (250 in allkeysdict)
       assert (75 in allkeysdict)
       assert ("waiting" in allkeysdict)
def test_searchWithExistingString_returnTrue():
    content = "foo bar in the connent "
    res=search("foo",content)
    print(res)
    assert res==True
def test_searchWithNonexistingWord_returnFalse():
    content = "foo bar in the connent "
    res=search("folo",content)
    print(res)
    assert res==False

def test_searchWithPreparecontent_ExistingTerm_returnTrue():
    content=preaparecontent("searchFile2.txt")
    searchTerm="ecomatic"
    res=search(searchTerm,content)
    print(res)
    assert res==True
def test_searchWithPreparecontent_NonexistingTerm_returnFalse():
    content=preaparecontent("searchFile2.txt")
    searchTerm="foo"
    res=search(searchTerm,content)
    print(res)
    assert res==False

def test_searchWithMissingSearchterm_returnFalse():
     searchTerm = ""
     content="hai hello with tsesting"
     res=search(searchTerm,content)
     print(res)
     assert res==False

def test_search_inFileWithMissingSearchterm_returnFalse():
    content=preaparecontent("searchFile2.txt")
    searchTerm=""
    res=search(searchTerm,content)
    print(res)
    assert res==False

def test_getfileWithRelativepath_returnFileList():
    file=getfiles(".")
    print(file)
    assert type(file)==list
def test_getfilesWithAbsalutePath_returnFilelist():
    filelist=getfiles(folderpath="C:\\Users\invvan\PycharmProjects\pythonProject")
    print(filelist)
    assert type(filelist)==list


# def test_getfile_with_CWD():
#     files = getfiles(".")
#     print(files)
#     searchTerm = "again"
#     for i in files:
#         content = preaparecontent(i)
#         res=search(searchTerm,content)
#         print("\n", content[:100], "...")
#         #assert res
#          if not res:
#             print('search failed for this')
#         else:
#             print('search passed for this')
#

def test_preparecontentWithJsonFile_returnJsonContent():
    content=preaparecontent("jsonstring.json")
    temp_content=content
    print(temp_content)
    assert temp_content==content

def test_preparecontentWithTXTFile_returnTxtcontent():
    content = preaparecontent("searchFile2.txt")
    temp_content = content
    print(temp_content)
    assert temp_content == content


def test_searchWithNotexistingWord_returnFalse():
     content = '{"foo":{"blah":1}, "bar":3}'
     res=search("bat",content)
     print(res)
     assert res==False
def test_search_WithExistingSingleAlphbet_returnTrue():
    searchTerm="e"
    content="gtrdhdeasedrtbhjikltoilpbfrwwxsabase"
    res=search(searchTerm,content)
    print(res)
    assert res==True
def test_search_WithNonexistingSingleAlphbet_returnFalse():
    searchTerm="z"
    content="gtrdhdeasedrtbhjikltoilpbfrwwxsabase"
    res=search(searchTerm,content)
    print(res)
    assert res==False

def test_search_WithExistingSingleAlphbet_WithPreparecontent_returnTrue():
    searchTerm="e"
    content=preaparecontent("searchFile2.txt")
    res = search(searchTerm, content)
    print(res)
    assert res==True

def test_search_WithExistingSingleAlphbet_WithPreparecontent_returnFalse():
    searchTerm="z"
    content=preaparecontent("searchFile2.txt")
    res = search(searchTerm, content)
    print(res)
    assert res==False


#simple search inside a html file
def test_searchInHtmlFileWithExistingString_returnTrue():
    content=preaparecontent("hai.html")
    searchTerm = "en"
    res=search(searchTerm,content)
    print(res)
    assert res==True
def test_searchInHtmlFileWithNonexistingString_returnFalse():
    content=preaparecontent("hai.html")
    searchTerm = "zn"
    res=search(searchTerm,content)
    print(res)
    assert res==False

#test with string start with upper and end with lower

def test_searchCaseInSesitive_returnTrue():
    content = "lang in the folder"
    searchTerm = "laNG"
    res=search(searchTerm, content)
    print(res)
    assert res==True

def test_searchExistingSubstring_returnTrue():
        content = "hello"
        searchTerm = "llo"
        res = search(searchTerm, content)
        print(res)
        assert res == True


def test_search_NonexistingSubstring_returnFalse():
    content = "hello"
    searchTerm = "kl"
    res = search(searchTerm, content)
    print(res)
    assert res == False


def test_searchExistingSubstring_Withprepareconent_returnTrue():
    content=preaparecontent("searchFile2.txt")
    searchTerm = "matic"
    res = search(searchTerm, content)
    print(res)
    assert res == True


def test_search_NonexistingSubstring_Withprepareconent_returnFalse():
    content=preaparecontent("searchFile2.txt")
    searchTerm = "yy"
    res = search(searchTerm, content)
    print(res)
    assert res == False


def test_searchUnicodeSearchterm_returnTrue():
    searchTerm = "U+0021"
    content="is this saerch can unicode U+0021 of a value!"
    res=search(searchTerm,content)
    print(res)
    assert res==True



def test_searchAlphaNumericValue_returnTrue():
    searchTerm="4F8"
    content="we need to search alphanumeric value like4F8"
    res=search(searchTerm,content)
    print(res)
    assert res==True

def test_getfileWithDirectfilename_returnNone():
    file = getfiles(folderpath="sample-2mb-text-file.tx1")
    print(file)
    assert file==None

def test_getfileByPassing_18FileLIstFolder_return18Filelist():
    file=getfiles(folderpath="C:\\Users\invvan\PycharmProjects\pythonProject\pythontest\stuff")
    print(file)
    assert len(file)==18



#passing an invalid file to the prapare content
def test_preparecontentWithNonexistingFile_rejectedWithFileNotFoundError():
    with pytest.raises(FileNotFoundError):
      filename="num.txt"
      create_connent=preaparecontent(filename)
      assert type(create_connent)
#testin an invalid directory
def test_getfileWithNonexistingDirectory_returnNone():
    path="C:\\Users\invvan\PycharmProjects\pythonProject\\rtt"
    inv_dir=getfiles(folderpath=path)
    print(inv_dir)
    assert inv_dir==None

#testin getfiles with empty directory
def test_getfileWithEmptyDir_returnNone():
    path="C:\\Users\invvan\PycharmProjects\pythonProject\venv\empty"
    empty_dir=getfiles(path)
    print(empty_dir )
    assert empty_dir==None
def test_getfileWithMissingFilePath_returnNone():
    path=""
    empty_path=getfiles(path)
    print(empty_path)
    assert empty_path==None

# #testin a temp directory
# def test_getfile_WithtempDirectory_returnEmptylist():
#     f = tempfile.TemporaryDirectory(dir="C:/")
#     file=getfiles(f)
#     print(file)
#     assert file==None
def test_prepareconent_withTempfile_returnNone():
    ff = tempfile.TemporaryFile(mode='w+')
    for i in ff:
        content=preaparecontent(i)
        print(content)
        ff.close()
        assert content==' '


#passing an html file to prepare content
def test_preparecontentWithHtmlFile_returnContentOfhtmlFile():
    filename = "hai.html"
    create_connent = preaparecontent(filename)
    print(create_connent)
    # temp=create_connent
    assert True
#test with pdf files
@pytest.mark.skip(reason="implement this feature later")
def test_preparecontentWithPdFile_returnPdfContent():
      filename="htsm.pdf"
      create_connent=preaparecontent(filename)
      print(create_connent)




# #writing into a file test them
# def test_file_textWrite():
#      f = open("searchFile1.txt", "w")
#      f.writelines("koii")
#      file="searchFile1.txt"
#      create_conent = preaparecontent(file)
#      print(create_conent)
#      res= search("helloworld",file)
#      f.close()
#      assert res==True
def test_searchIndexOneWithSpace_retuenTrue():
    searchTerm = " hello"
    content=" hey hello"
    res=search(searchTerm,content)
    print(res)
    assert res==True
def test_searchMiddleindexWithSpace_returnTrue():
    content=" hello"
    searchTerm="he llo"
    res=search(searchTerm,content)
    print(res)
    assert res==True
@pytest.mark.skip(reason="can search this or not")
def test_preparecontentWithExcelFile_retuenExcelcontent():
     content=preaparecontent("Book1.xlsx")
     print(content)




def test_searchIncaseSensitive_returnFalse():
        stringcontent= "Again in the folder"
        searchTerm="again"
        res=search(searchTerm,stringcontent)
        assert res== False
#test prepare content of 34,674,573 word file

def test_lodtestWith_preparecontent():
    preaparecontent("tracker004_track_2015-08-28_22-22-01-238000.json")

def test_searchIncaseSensitive_WithPreparecontent_returnTrue():
    searchTerm="agaIN"
    content=preaparecontent("searchFile4.txt")
    assert search(searchTerm,content) == True
#test with 1kb txt file
def test_preparecontentLodtest_With1KBtxtFile():
    searchTerm = "non"
    content = preaparecontent("sample-text-file.txt")
    res=search(searchTerm, content)
    print(res)
    assert res ==True
#test with 2mb txt file
def test_preparecontentLodtest_With2MBtxtFile():
    searchTerm = "amet"
    content = preaparecontent("sample-2mb-text-file.txt")
    assert search(searchTerm, content) ==True

@pytest.mark.parametrize('searchTerm',[("."),(","),("~"),("`"),("<"),(">"),("/"),("?"),(";"),(":"),("\'"),("\""),("["),("]"),("{"),("}"),('"\"'),("|"),("="),("+"),("-"),("_"),("*"),("\()"),("&"),("^"),("%"),("$"),("#"),("@")])
def test_canSearchSpecialCharcter_returnTrue(searchTerm):
    content="helloworld,'!#$%&'()*+,-./:;<=>?@[\]^_`{|}~  with the help of ;blackbox* 'testing."
    termsearch=search(searchTerm,content)
    print(termsearch)
    assert 1

@pytest.mark.parametrize('searchTerm',
                             [("."), (","), ("~"), ("`"), ("<"), (">"), ("/"), ("?"), (";"), (":"), ("\'"), ("\""),
                              ("["), ("]"), ("{"), ("}"), ('"\"'), ("|"), ("="), ("+"), ("-"), ("_"), ("*"), ("\()"),
                              ("&"), ("^"), ("%"), ("$"), ("#"), ("@")])
def test_canSearchSpecialCharcter_WithPreparecontent_returnTrue(searchTerm):
        content = preaparecontent("jsonstring.json")
        termsearch = search(searchTerm, content)
        print(termsearch)
        assert 1
@pytest.mark.parametrize('searchTerm',[("a"),("e"),("r"),("o"),("i"),("m"),("l"),("k")])
def test_searchMultipleSingleAlphabet_returnTrue(searchTerm):
    content = "helloworld with the me help of ;blackbox* 'testing."
    termsearch = search(searchTerm, content)
    print(termsearch)
    assert 1
@pytest.mark.parametrize('searchTerm',[("a"),("e"),("r"),("o"),("i"),("m"),("l"),("k")])
def test_searchMultipleSingleAlphabet_WithPreparecontent_returnTrue(searchTerm):
    content =preaparecontent("jsonstring.json")
    termsearch = search(searchTerm, content)
    print(termsearch)
    assert 1

def test_jsearchWithDirectJsonSearchtem_returnFalse():
    content= '{"foo":{"blah":1}, "bar":3}'
    res= jsearch(content).searchanykey(searchTerm='{"blah":1}')
    print(res)
    assert res == False
#passing a string as Serachterm to a jsearch
def test_jsearchWithStringAsjcontent_rejectedWithValueError():
     with pytest.raises(ValueError):
        content = "passing searchterm directly as json value"
        res = jsearch(content).searchanykey(searchTerm="passing")
        print(res)
        assert res==True


if __name__=="__main__":
    if __name__ == '__main__':
        #pytest.main()
        pytest.main(args=['--verbose'])





















